import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Navigacija from '../Navigacija/Navigacija';
import NastavnikPrikazSvih from '../Nastavnik/NastavnikPrikazSvih';
import NastanikKreiraj from '../Nastavnik/NastavnikKreiraj';
import NastavnikIzmena from '../Nastavnik/NastavnikIzmena';
import { Container } from 'react-bootstrap';
import PokrivenostNastaveKreiraj from '../PokrivenostNastave/PokrivenostNastaveKreiraj';
import PokrivenostNastavePrikazSvih from '../PokrivenostNastave/PokrivenostNastavePrikazSvih';
import PokrivenostNastaveIzmeni from '../PokrivenostNastave/PokrivenostNastaveIzmeni';

function App() {
    return (
        <BrowserRouter>
            <Navigacija />
            <Container style={{ marginBottom: '50px' }}>
                <Switch>
                    <Route exact path="/" component={NastavnikPrikazSvih} />
                    <Route path="/nastavnici" exact component={NastavnikPrikazSvih} />
                    <Route path="/nastavnici/kreiraj" exact component={NastanikKreiraj} />
                    <Route path="/nastavnici/izmeni/:id" exact component={NastavnikIzmena} />
                    <Route path="/pokrivenost-nastave" exact component={PokrivenostNastavePrikazSvih} />
                    <Route path="/pokrivenost-nastave/izmeni/:id" exact component={PokrivenostNastaveIzmeni} />
                    <Route path="/pokrivenost-nastave/kreiraj" exact component={PokrivenostNastaveKreiraj} />
                    <Route component={() => <h1>404 - Страница није пронађена</h1>} />
                </Switch>
            </Container>
        </BrowserRouter>
    );
}

export default App;
