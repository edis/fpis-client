import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Navbar, Nav } from 'react-bootstrap'
class Navigacija extends Component {

    render() {
        return (
            <nav>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand>ФПИС - ФОН</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Link to="/" className="nav-link">Почетна</Link>
                            <Link to="/nastavnici" className="nav-link">Наставници</Link>
                            <Link to="/pokrivenost-nastave" className="nav-link">Покривеност наставе</Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </nav>
        );
    }
}

export default Navigacija;