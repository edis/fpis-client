import React, { Component } from 'react';
import axios from 'axios';
import { BASE_URL, NASTAVNIK } from '../util/apiUrls';
import { Button, CircularProgress, TextField } from '@material-ui/core';
import { Form, Row, Col, Container, Alert } from 'react-bootstrap';
import Header from '../Header/Header';

class NastavnikIzmena extends Component {
    state = {
        id: null,
        ime: null,
        prezime: null,
        jmbg: null,
        message: null,
        hasError: false,
    }

    ucitajNastavnika = () => {
        const { id } = this.props.match.params;

        axios.get(BASE_URL + NASTAVNIK + '/' + id)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    if (resp.data.data != null) {
                        const { id, ime, prezime, jmbg } = resp.data.data;
                        this.setState({
                            id: id,
                            ime: ime,
                            prezime: prezime,
                            jmbg: jmbg,
                        });
                    } else {
                        this.props.history.push('/nastavnici/');
                    }
                } else {
                    this.props.history.push('/nastavnici/');
                }
            }).catch(err => {
                this.props.history.push('/nastavnici/');
            });
    }

    submitForm = (e) => {
        e.preventDefault();

        const { id, ime, prezime, jmbg } = this.state;

        const nastavnik = {
            id: id,
            ime: ime.trim(),
            prezime: prezime.trim(),
            jmbg: jmbg,
        }

        axios.put(BASE_URL + NASTAVNIK + '/' + id, nastavnik).then(resp => {
            const { status } = resp.data;
            if (status === 200) {
                this.setMessage('Наставник је успешно ажуриран.');
            } else {
                this.setMessage(resp.data.message, true);
            }
        }).catch(err => {
            this.setMessage('Дошло је до грешке приликом ажурирања наставника', true);
        });
    }

    setMessage = (msg, hasError = false) => {
        this.setState({
            message: msg,
            hasError: hasError,
        });
    }

    componentDidMount() {
        this.ucitajNastavnika();
    }

    renderMessage = () => {
        const { message, hasError } = this.state;
        if (message === null) {
            return null;
        }

        return (
            <Alert variant={hasError ? 'danger' : 'success'}>
                {message}
            </Alert>
        );
    }

    renderForm = () => {
        const { id } = this.state;
        if (id == null) {
            return <CircularProgress />;
        }

        return (
            <div>
                {this.renderMessage()}
                <Form onSubmit={this.submitForm}>
                    <Container>
                        <Row style={{ marginBottom: '20px' }}>
                            <Col xs={12}>
                                <TextField type="text"
                                    label="Име"
                                    value={this.state.ime}
                                    onChange={(e) => this.setState({ ime: e.target.value })}
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>

                        </Row>
                        <Row style={{ marginBottom: '20px' }}>
                            <Col xs={12}>
                                <TextField
                                    type="text"
                                    value={this.state.prezime}
                                    onChange={(e) => this.setState({ prezime: e.target.value })}
                                    label="Презиме"
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: '20px' }}>
                            <Col xs={12}>
                                <TextField
                                    type="text"
                                    style={{ disabled: 'true' }}
                                    value={this.state.jmbg}
                                    label="ЈМБГ"
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <Button variant="contained" color="default" type="submit" fullWidth>
                                    Измени
                            </Button>
                            </Col>
                        </Row>
                    </Container>
                </Form>
            </div>
        );
    }

    render() {
        return (
            <div>
                <Header text='Измена изабраног наставника' />
                {this.renderForm()}
            </div>
        );
    }
}

export default NastavnikIzmena;