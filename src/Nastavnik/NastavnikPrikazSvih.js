import React, { Component } from 'react';
import axios from 'axios';
import { BASE_URL, NASTAVNIK } from '../util/apiUrls';
import Header from '../Header/Header';
import { Button, CircularProgress, Paper, Table, TableBody, TableRow, TableCell, Checkbox, TableHead } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';

class NastavnikPrikazSvih extends Component {
    state = {
        izabraniNastavnik: null,
        nastavnici: null,
        loading: true,
        error: null,
    }

    ucitajNastavnike = () => {
        axios.get(BASE_URL + NASTAVNIK)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.setData(resp.data.data);
                } else {
                    this.setError(resp.data.message);
                }
            }).catch(err => {
                this.setError('Дошло је до грешке прилком учитавања наставника.');
            });
    }

    componentDidMount() {
        this.ucitajNastavnike();
    }

    setData = (data) => {
        this.setState({
            nastavnici: data,
            error: null,
            loading: false,
            izabraniNastavnik: null,
        });
    }

    setError = (error) => {
        this.setState({
            nastavnici: null,
            error: error,
            loading: false,
        });
    }

    changeSelectedRow = (nastavnik) => {
        const { izabraniNastavnik } = this.state;

        if (izabraniNastavnik === nastavnik) {
            this.setState({
                izabraniNastavnik: null
            });
        } else {
            this.setState({
                izabraniNastavnik: nastavnik
            });
        }
    }

    obrisiNastavnika = (nastavnik) => {
        axios.delete(BASE_URL + NASTAVNIK + '/' + nastavnik.id)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.ucitajNastavnike();
                } else {
                    alert(resp.data.message);
                }
            }).catch(err => {
                alert('Дошло је до грешке приликом брисања наставника.');
            });
    }

    izmeniNastavnika = (nastavnik) => {
        this.props.history.push('/nastavnici/izmeni/'+nastavnik.id);
    }

    renderNastavnici = () => {
        const { nastavnici, loading, error } = this.state;

        if (loading) {
            return <CircularProgress />;
        }

        if (error !== null) {
            return error;
        }

        if (nastavnici === null || nastavnici.length === 0) {
            return (
                <div>
                    Тренутно нема наставника.
                </div>
            );
        }

        return (
            <Paper style={{ marginTop: '20px' }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Име</TableCell>
                            <TableCell>Презиме</TableCell>
                            <TableCell>ЈМБГ</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            nastavnici.map(nastavnik => {
                                return (
                                    <TableRow
                                        hover
                                        onClick={() => this.changeSelectedRow(nastavnik)}
                                        key={nastavnik.id}
                                        selected={this.state.izabraniNastavnik === nastavnik}
                                    >
                                        <TableCell style={{ width: '10%' }}>
                                            <Checkbox checked={this.state.izabraniNastavnik === nastavnik} />
                                        </TableCell>

                                        <TableCell>{nastavnik.ime}</TableCell>
                                        <TableCell>{nastavnik.prezime}</TableCell>
                                        <TableCell>{nastavnik.jmbg}</TableCell>
                                        <TableCell>
                                            <Button onClick={() => this.izmeniNastavnika(nastavnik)}>
                                                <EditIcon />
                                            </Button>
                                            <Button onClick={() => this.obrisiNastavnika(nastavnik)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </Paper>
        );
    }

    noviNastavnik = () => {
        this.props.history.push('/nastavnici/kreiraj');
    }

    render() {
        return (
            <div>
                <Header text="Приказ свих наставника" />

                <Button onClick={this.noviNastavnik} variant="contained" title="Креирај новог наставника">
                    Креирај новог наставника
                </Button>

                <div>
                    {
                        this.renderNastavnici()
                    }
                </div>
            </div>
        );
    }
}

export default NastavnikPrikazSvih;