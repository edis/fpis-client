import React, { Component } from 'react';
import Header from './../Header/Header'
import { Form, Alert, Row, Col, Container } from 'react-bootstrap';
import { Button, TextField } from '@material-ui/core'
import axios from 'axios';
import { BASE_URL, NASTAVNIK } from '../util/apiUrls';

class NastanikKreiraj extends Component {

    state = {
        ime: '',
        prezime: '',
        jmbg: parseInt(Math.random()*10000),
        message: null,
        hasError: false,
    }

    getInitJmbg = () => {
        return parseInt(Math.random()*10000);
    }

    submitForm = (e) => {
        e.preventDefault();

        const { ime, prezime, jmbg } = this.state;

        const nastavnik = {
            ime: ime.trim(),
            prezime: prezime.trim(),
            jmbg: jmbg.toString().trim(),
        }

        axios.post(BASE_URL + NASTAVNIK, nastavnik).then(resp => {
            const { status } = resp.data;
            if (status === 200) {
                this.setMessage('Наставник је успешно креиран.');
            } else {
                this.setMessage(resp.data.message, true);
            }
        }).catch(err => {
            this.setMessage('Дошло је до грешке приликом креирања наставника', true);
        });
    }

    setMessage = (msg, hasError = false) => {
        if (!hasError) {
            this.setState({
                ime: '',
                prezime: '',
                jmbg: this.getInitJmbg(),
            });
        }
        this.setState({
            message: msg,
            hasError: hasError,
        });
    }

    renderMessage = () => {
        const { message, hasError } = this.state;
        if (message === null) {
            return null;
        }

        return (
            <Alert variant={hasError ? 'danger' : 'success'}>
                {message}
            </Alert>
        );
    }

    render() {
        return (
            <div>
                <Header text='Креирање новог наставника' />
                {
                    this.renderMessage()
                }
                <Form onSubmit={this.submitForm}>
                    <Container>
                        <Row style={{marginBottom: '20px'}}>
                            <Col xs={12}>
                                <TextField type="text"
                                    label="Име"
                                    value={this.state.ime}
                                    onChange={(e) => this.setState({ ime: e.target.value })}
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>

                        </Row>
                        <Row style={{marginBottom: '20px'}}>
                            <Col xs={12}>
                                <TextField
                                    type="text"
                                    value={this.state.prezime}
                                    onChange={(e) => this.setState({ prezime: e.target.value })}
                                    label="Презиме"
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>
                        </Row>
                        <Row style={{marginBottom: '20px'}}>
                            <Col xs={12}>
                                <TextField
                                    type="text"
                                    value={this.state.jmbg}
                                    onChange={(e) => this.setState({ jmbg: e.target.value })}
                                    label="ЈМБГ"
                                    autoComplete='off'
                                    variant="outlined"
                                    fullWidth
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <Button variant="contained" color="default" type="submit" fullWidth>
                                    Креирај
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Form>
            </div>
        );
    }
}

export default NastanikKreiraj;