import React, { Component } from 'react';
import { Tabs, Tab, Select, MenuItem, TextField, Paper, FormControl, Button, Checkbox, Table, TableHead, TableBody, TableRow, TableCell, CircularProgress, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography } from '@material-ui/core';
import axios from 'axios';
import { BASE_URL, BEAN_NASTAVNI_PLAN, BEAN_LITERATURA, BEAN_NASTAVNIK, BEAN_PREDMET, BEAN_TIP_AKTIVNOSTI, POKRIVENOST_NASTAVE } from '../util/apiUrls';
import { Row, Col, Alert } from 'react-bootstrap';
import Header from '../Header/Header';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined'
import SaveIcon from '@material-ui/icons/Save'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';


class PokrivenostNastaveIzmeni extends Component {
    state = {
        loading: true,
        hasError: false,
        message: null,
        tab: 0,

        kreiranPokrivenostNastave: false,

        ucitaniPodaci: {
            nastavniPlanovi: null,
            literature: null,
            nastavnici: null,
            predmeti: null,
            tipoviAktivnosti: null,
        },

        stavkaZaIzmenu: null,

        izabraniNastavniPlan: null,
        izabranaLiteratura: null,
        izabraniNastavnik: null,
        izabraniPredmet: null,
        izabraniTipAktivnosti: null,
        pravoPotpisa: false,

        pokrivenostNastave: null,

        dodataLiteratura: [],
        dodataAngazovanja: [],
        dodateStavkePokrivenostiNastave: [],

        godina: new Date().getFullYear() + '/' + (new Date().getFullYear() + 1),
        datum: null,
    }

    ucitaj = (naziv, uri) => {
        axios.get(BASE_URL + uri).then(resp => {
            const { status } = resp.data;
            if (status === 200) {
                const { ucitaniPodaci } = this.state;
                ucitaniPodaci[naziv] = resp.data.data;
                this.setState({
                    ucitaniPodaci,
                });
            } else {
                this.setLoadingError();
            }
        }).catch(err => {
            this.setLoadingError();
        });
    }

    setLoadingError = () => {
        this.setState({
            hasError: true,
            loading: false,
        });
    }

    ucitajPokrivenostNastave = () => {
        const { id } = this.props.match.params;

        axios.get(BASE_URL + POKRIVENOST_NASTAVE + '/' + id)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.setState({
                        loading: false,
                        hasError: false,
                        pokrivenostNastave: resp.data.data,
                    });
                } else {
                    this.props.history.push('/');
                }
            }).catch(err => {
                this.props.history.push('/');
            });
    }

    componentDidMount() {
        this.ucitajPokrivenostNastave();
        this.setState({
            datum: this.getTekuciDatum(),
        });
        try {
            this.ucitaj('nastavniPlanovi', BEAN_NASTAVNI_PLAN);
            this.ucitaj('literature', BEAN_LITERATURA);
            this.ucitaj('nastavnici', BEAN_NASTAVNIK);
            this.ucitaj('predmeti', BEAN_PREDMET);
            this.ucitaj('tipoviAktivnosti', BEAN_TIP_AKTIVNOSTI);
        } catch (error) {
            this.setLoadingError();
        }

    }

    showMessage = (msg, type = 'success') => {
        if (msg == null) {
            this.setState({
                message: null,
            })
        } else {
            this.setState({
                message: (
                    <Alert variant={type}>
                        {msg}
                    </Alert>
                )
            });
        }
    }

    sacuvajPokrivenostNastave = () => {
        const { pokrivenostNastave, dodateStavkePokrivenostiNastave } = this.state;

        pokrivenostNastave['stavke'] = dodateStavkePokrivenostiNastave;

        axios.post(BASE_URL + POKRIVENOST_NASTAVE, pokrivenostNastave)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.showMessage('Покривеност наставе је успешно креирана');
                } else {
                    this.showMessage(resp.data.message, 'danger');
                }
            }).catch(err => {
                alert(err);
            });
    }

    izaberiNastavniPlan = (e) => {
        const { nastavniPlanovi } = this.state.ucitaniPodaci;
        const nastavniPlan = nastavniPlanovi.find((el) => el.id === e.target.value);

        const { pokrivenostNastave } = this.state;

        const novaPokrivenostNastave = {
            ...pokrivenostNastave
        }
        novaPokrivenostNastave.nastavniPlan = {
            ...nastavniPlan
        }

        if (novaPokrivenostNastave && novaPokrivenostNastave.nastavniPlan) {
            this.setState({
                pokrivenostNastave: novaPokrivenostNastave
            });
        }
    }

    getNastavniPlanValue = (nastavniPlan) => {
        if (nastavniPlan === null) {
            return '';
        }
        return nastavniPlan.datumOd + ' - ' + nastavniPlan.datumDo;
    }

    setTekucaGodina = () => {
        const year = new Date().getFullYear();
        this.setState({
            godina: year + '/' + (year + 1),
        })
    }

    renderIzborNastavnogPlana = () => {
        const { pokrivenostNastave } = this.state;
        const { nastavniPlan } = pokrivenostNastave;
        const { nastavniPlanovi } = this.state.ucitaniPodaci;

        if (nastavniPlanovi === null) {
            return null;
        }

        return (
            <Row style={{marginTop: '10px', marginBottom: '10px'}}>
                <Col xs={3}>
                    Наставни план
                </Col>
                <Col xs={9}>
                    <FormControl fullWidth>
                        <Select
                            value={this.getNastavniPlanValue(nastavniPlan)}
                            onChange={this.izaberiNastavniPlan}
                            renderValue={() => this.getNastavniPlanValue(nastavniPlan)}>
                            {
                                nastavniPlanovi.map(nastavniPlan => {
                                    return (
                                        <MenuItem key={nastavniPlan.id} value={nastavniPlan.id}>
                                            {this.getNastavniPlanValue(nastavniPlan)}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    renderUnosGodine = () => {
        return (
            <Row>
                <Col xs={3}>
                    Година
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <TextField
                            label='Година'
                            variant="outlined"
                            value={this.state.godina}
                            onChange={(e) => this.setState({ godina: e.target.value })}
                        />
                    </FormControl>
                </Col>
                <Col>
                    <Button size="small" variant="text" onClick={this.setTekucaGodina}>
                        Текућа година
                    </Button>
                </Col>
            </Row>
        );
    }

    getTekuciDatum = () => {
        const tekuciDatum = new Date();

        const month = tekuciDatum.getMonth() + 1;
        return tekuciDatum.getFullYear() + '-' + ((month < 10) ? '0' + month : month) + '-' + tekuciDatum.getDate();
    }

    renderIzborDatuma = () => {


        return (
            <Row>
                <Col xs={3}>
                    Датум
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <TextField
                            id="date"
                            label="Датум"
                            type="date"
                            defaultValue={this.getTekuciDatum()}
                            onChange={(e) => this.setState({ datum: e.target.value })}
                        />
                    </FormControl>
                </Col>
            </Row>
        );
    }

    napraviPokrivenostNastave = () => {
        const { izabraniNastavniPlan, godina, datum } = this.state;

        if (izabraniNastavniPlan === null || godina === null || datum === null || godina.toString().trim() === '' || datum.trim() === '') {
            alert('Сва поља су обавезна');
            return;
        }

        const pokrivenostNastave = {
            nastavniPlan: izabraniNastavniPlan,
            godina,
            datum,
        }

        this.setState({
            pokrivenostNastave: pokrivenostNastave,
            kreiranPokrivenostNastave: true,
        });
    }

    getPredmetValue = (predmet) => {
        if (predmet === null) {
            return '';
        }
        return predmet.naziv;
    }

    getNastavniciValue = (angazovanja) => {
        var value = '';
        angazovanja.forEach(angazovanje => {
            value += angazovanje.nastavnik.ime + ' ' + angazovanje.nastavnik.prezime + ' (' + angazovanje.tipAktivnosti.naziv + ')';
        });
        return value.length > 50 ? <div title={value}>{value.substr(0, 47)}...</div> : value;
    }

    getLiteraturaValue = (literature) => {
        var value = '';
        literature.forEach(literatura => {
            value += literatura.naziv + ' ';
        });
        return value.length > 50 ? <div title={value}>{value.substr(0, 47)}...</div> : value;
    }

    renderNapravljeneStavke = () => {
        const { dodateStavkePokrivenostiNastave } = this.state;
        if (dodateStavkePokrivenostiNastave === null || dodateStavkePokrivenostiNastave.length === 0) {
            return null;
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Предмет</TableCell>
                            <TableCell>Наставници</TableCell>
                            <TableCell>Литература</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodateStavkePokrivenostiNastave.map(stavka => {
                                return (
                                    <TableRow key={Math.random()}>
                                        <TableCell>{this.getPredmetValue(stavka.predmet)}</TableCell>
                                        <TableCell>{this.getNastavniciValue(stavka.angazovanja)}</TableCell>
                                        <TableCell>{this.getLiteraturaValue(stavka.literatura)}</TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    renderNapravljenPokrivenostNastave = () => {
        const { pokrivenostNastave } = this.state;
        if (pokrivenostNastave === null) {
            return null;
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Наставни план</TableCell>
                            <TableCell>Година</TableCell>
                            <TableCell>Датум</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>{this.getNastavniPlanValue(pokrivenostNastave.nastavniPlan)}</TableCell>
                            <TableCell>{pokrivenostNastave.godina}</TableCell>
                            <TableCell>{pokrivenostNastave.datum}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                {this.renderNapravljeneStavke()}

                <Button variant="outlined" style={{ margin: '20px' }} onClick={this.sacuvajPokrivenostNastave}>
                    <SaveIcon />
                </Button>
            </div>
        );
    }

    renderAngazovanja = (angazovanja, tipNastave) => {
        const angazovanjaVezbe = angazovanja.filter(angazovanje => angazovanje.tipAktivnosti.naziv === tipNastave);

        if (angazovanjaVezbe.length === 0) {
            return '-';
        }

        return angazovanjaVezbe.map(angazovanje => {
            const { nastavnik } = angazovanje;
            return (
                <div key={angazovanje.id + '-' + nastavnik.id + '-' + Math.random()}>
                    {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                </div>
            );
        });
    }

    obrisiLiteraturuZaStavku = (stavkaId, literaturaId) => {
        const { pokrivenostNastave } = this.state;

        const { stavke } = pokrivenostNastave;
        const stavka = stavke.find(stavka => stavka.id === stavkaId);

        const noveLiterature = stavka.literatura.filter(literatura => literatura.id !== literaturaId);
        stavka.literatura = noveLiterature;
        this.setState({
            pokrivenostNastave: {
                ...pokrivenostNastave
            }
        });

    }

    renderLiteratura = (literature) => {
        if (literature.length === 0) {
            return '-';
        }

        return literature.map(literatura => {
            return (
                <div key={literatura.id + '-' + literatura.predmet.id + '-' + Math.random()}>
                    {literatura.naziv}
                </div>
            );
        });
    }

    obrisiStavku = (stavka) => {
        const { pokrivenostNastave } = this.state;
        const { stavke } = pokrivenostNastave;

        const noveStavke = stavke.filter(st => st.id !== stavka.id);

        pokrivenostNastave.stavke = noveStavke;

        this.setState({
            pokrivenostNastave: {
                ...pokrivenostNastave
            }
        });
    }

    setStavkaZaIzmenu = (stavka) => {
        this.setState({
            stavkaZaIzmenu: {
                ...stavka
            },
            tab: 1,
        });
    }

    renderStavke = (stavke) => {
        if (stavke === null) {
            return null;
        }

        return (
            <Paper style={{ overflow: 'auto' }}>
                {
                    stavke.map(stavka => {
                        const { angazovanja, literatura } = stavka;
                        return (
                            <ExpansionPanel key={stavka.id}>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography>
                                        {stavka.predmet.naziv}
                                    </Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Назив предмета</TableCell>
                                                <TableCell>Предавања</TableCell>
                                                <TableCell>Вежбе</TableCell>
                                                <TableCell>Лабораторијске вежбе</TableCell>
                                                <TableCell>Литература</TableCell>
                                                <TableCell>Акција</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell>
                                                    {stavka.predmet.naziv}
                                                </TableCell>
                                                <TableCell>
                                                    {this.renderAngazovanja(angazovanja, 'predavanja')}
                                                </TableCell>
                                                <TableCell>
                                                    {this.renderAngazovanja(angazovanja, 'vezbe')}
                                                </TableCell>
                                                <TableCell>
                                                    {this.renderAngazovanja(angazovanja, 'laboratorijske vezbe')}
                                                </TableCell>
                                                <TableCell>
                                                    {this.renderLiteratura(literatura)}
                                                </TableCell>
                                                <TableCell>
                                                    <Button variant="text" onClick={() => this.setStavkaZaIzmenu(stavka)}>
                                                        <EditIcon />
                                                    </Button>
                                                    <Button variant="text" onClick={() => this.obrisiStavku(stavka)}>
                                                        <DeleteIcon />
                                                    </Button>
                                                </TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        );
                    })
                }
            </Paper>
        );
    }

    sacuvajIzmene = () => {
        const { pokrivenostNastave } = this.state;

        axios.put(BASE_URL + POKRIVENOST_NASTAVE, pokrivenostNastave)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.props.history.push('/pokrivenost-nastave')
                } else {
                    alert(resp.data.message);
                }
            }).catch(err => {
                this.props.history.push('/pokrivenost-nastave/izmeni/' + pokrivenostNastave.id)
            })
    }

    renderPokrivenostNastave = () => {
        const { loading, hasError, pokrivenostNastave } = this.state;

        if (loading) {
            return <CircularProgress />
        }

        if (hasError) {
            return 'Дошло је до грешке приликом учитавања података.';
        }

        return (
            <div>
                <div>
                    <h3 className="center">Покривеност наставе за школску

                    <FormControl>
                            <TextField
                                label='Година'
                                variant="outlined"
                                value={this.state.pokrivenostNastave.godina}
                                onChange={(e) => {
                                    const godina = e.target.value;
                                    const { pokrivenostNastave } = this.state;
                                    pokrivenostNastave.godina = godina;
                                    this.setState({
                                        pokrivenostNastave: pokrivenostNastave,
                                    })
                                }}
                            />
                        </FormControl>

                        годину</h3>


                    {this.renderIzborNastavnogPlana()}


                    <Row>
                        <Col>
                            {
                                this.renderStavke(pokrivenostNastave.stavke)
                            }
                        </Col>
                    </Row>

                    <Button variant="contained" onClick={this.sacuvajIzmene} style={{ marginTop: '20px' }}>
                        Сачувај измене
                    </Button>
                </div>
            </div>
        );
    }

    renderFormPokrivenostNastave = () => {
        const marginBottom20 = {
            marginBottom: '20px',
        }

        return (
            <Paper style={{ padding: '20px' }}>
                <h2>
                    Покривеност наставе
                </h2>
                <Row>
                    <Col>
                        <div style={marginBottom20}>
                            {
                                this.renderIzborNastavnogPlana()
                            }
                        </div>

                        <div style={marginBottom20}>
                            {
                                this.renderUnosGodine()
                            }
                        </div>

                        <div style={marginBottom20}>
                            {
                                this.renderIzborDatuma()
                            }
                        </div>
                        {
                            !this.state.kreiranPokrivenostNastave ? (
                                <Button variant="outlined" onClick={this.napraviPokrivenostNastave}>
                                    <SaveIcon />
                                </Button>
                            ) : null
                        }

                    </Col>
                    <Col>
                        {this.renderNapravljenPokrivenostNastave()}
                    </Col>
                </Row>
            </Paper>
        );
    }

    getIzabraniPredmetValue = () => {
        const { izabraniPredmet } = this.state;
        if (izabraniPredmet === null) {
            return 'Изабери предмет';
        }
        return this.getPredmetValue(izabraniPredmet);
    }

    izaberiPredmet = (e) => {
        const id = e.target.value;
        const { predmeti } = this.state.ucitaniPodaci;
        const predmet = predmeti.find(predmet => predmet.id === id);
        if (predmet !== null && predmet !== undefined) {
            this.setState({
                izabraniPredmet: predmet,
            });
        }
    }

    renderIzborPredmeta = () => {
        const { predmeti } = this.state.ucitaniPodaci;
        if (predmeti === null) {
            return null;
        }
        return (
            <Row>
                <Col xs={3}>
                    Предмет
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniPredmetValue()}
                            onChange={this.izaberiPredmet}
                            renderValue={this.getIzabraniPredmetValue}>
                            {
                                predmeti.map(predmet => {
                                    return (
                                        <MenuItem key={predmet.id} value={predmet.id}>
                                            {predmet.naziv}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    getIzabraniNastavnikValue = () => {
        const { izabraniNastavnik } = this.state;
        if (izabraniNastavnik === null) {
            return 'Изабери наставника';
        }
        return izabraniNastavnik.ime + ' ' + izabraniNastavnik.prezime + ' (' + izabraniNastavnik.jmbg + ')';
    }

    izaberiNastavnika = (e) => {
        const { nastavnici } = this.state.ucitaniPodaci;
        const id = e.target.value;
        const nastavnik = nastavnici.find((nastavnik) => nastavnik.id === id);
        if (nastavnik !== null && nastavnik !== undefined) {
            this.setState({
                izabraniNastavnik: nastavnik,
            });
        }
    }

    renderIzborNastavnika = () => {
        const { nastavnici } = this.state.ucitaniPodaci;
        if (nastavnici === null) {
            return null;
        }

        return (
            <Row>
                <Col xs={colXs4}>
                    Наставник
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniNastavnikValue()}
                            onChange={this.izaberiNastavnika}
                            renderValue={this.getIzabraniNastavnikValue}>
                            {
                                nastavnici.map(nastavnik => {
                                    return (
                                        <MenuItem key={nastavnik.id} value={nastavnik.id}>
                                            {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    getIzabraniTipAktivnostiValue = () => {
        const { izabraniTipAktivnosti } = this.state;
        if (izabraniTipAktivnosti === null) {
            return 'Изабери тип активности';
        }
        const obaveznaPrijava = (izabraniTipAktivnosti.obaveznaPrijava === 0) ? 'пријава није обавезна' : 'пријава је обавезна';
        return izabraniTipAktivnosti.naziv + ' (' + obaveznaPrijava + ')';
    }

    izaberiTipAktivnosti = (e) => {
        const id = e.target.value;
        const { tipoviAktivnosti } = this.state.ucitaniPodaci;
        const tipAktivnosti = tipoviAktivnosti.find(ta => ta.id === id);
        if (tipAktivnosti !== null && tipAktivnosti !== undefined) {
            this.setState({
                izabraniTipAktivnosti: tipAktivnosti,
            });
        }
    }

    renderIzborTipaAktivnosti = () => {
        const { tipoviAktivnosti } = this.state.ucitaniPodaci;
        if (tipoviAktivnosti === null) {
            return null;
        }

        return (
            <Row>
                <Col xs={colXs4}>
                    Тип активности
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniTipAktivnostiValue()}
                            onChange={this.izaberiTipAktivnosti}
                            renderValue={this.getIzabraniTipAktivnostiValue}>
                            {
                                tipoviAktivnosti.map(tipAktivnosti => {
                                    return (
                                        <MenuItem key={tipAktivnosti.id} value={tipAktivnosti.id}>
                                            {tipAktivnosti.naziv}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    renderPravoPotpisa = (source = 'stavka') => {
        const { pravoPotpisa } = this.state;
        return (
            <Row>
                <Col xs={3}>
                    Право потписа
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Checkbox
                            checked={pravoPotpisa}
                            onChange={() => this.setState({ pravoPotpisa: !pravoPotpisa })}
                        />
                    </FormControl>
                </Col>
                <Col>
                    <Button variant="outlined" size="small" onClick={() => this.dodajAngazovanje(source)}>
                        Додај ангажовање
                    </Button>
                </Col>
            </Row>
        );
    }

    dodajAngazovanje = (source) => {
        const { izabraniNastavnik, izabraniTipAktivnosti, pravoPotpisa } = this.state;
        const dodataAngazovanja = (source === 'stavka') ? this.state.dodataAngazovanja : this.state.stavkaZaIzmenu.angazovanja;

        if (izabraniNastavnik === null) {
            alert('Изабери наставника');
            return;
        }

        if (izabraniTipAktivnosti === null) {
            alert('Изабери тип активности');
            return;
        }

        const angazovanjeId = izabraniNastavnik.id + '' + izabraniTipAktivnosti.id;

        const postojeceAngazovanje = dodataAngazovanja.find(angazovanje => {
            const id = angazovanje.nastavnik.id + '' + angazovanje.tipAktivnosti.id;
            if (parseInt(id) === parseInt(angazovanjeId)) {
                return true;
            }
            return false;
        });
        if (postojeceAngazovanje !== null && postojeceAngazovanje !== undefined) {
            alert('Ангажовање са изабраним наставником и типом активности већ постоји');
            return;
        }

        const angazovanje = {
            id: parseInt(angazovanjeId),
            nastavnik: izabraniNastavnik,
            tipAktivnosti: izabraniTipAktivnosti,
            pravoPotpisa: pravoPotpisa,
        };

        const angazovanja = [...dodataAngazovanja];
        angazovanja.push(angazovanje);

        if (source === 'stavka') {
            this.setState({
                dodataAngazovanja: angazovanja
            });
        } else if (source === 'stavkaZaIzmenu') {
            const stavka = { ...this.state.stavkaZaIzmenu };
            stavka.angazovanja = angazovanja;
            this.setState({
                stavkaZaIzmenu: stavka
            });
        }

    }

    renderIzborAngazovanja = () => {
        return (
            <div>
                <h4>Додавање анагажовања</h4>

                {this.renderIzborNastavnika()}

                {this.renderIzborTipaAktivnosti()}

                {this.renderPravoPotpisa()}
            </div>
        );
    }

    obrisiDodatoAngazovanje = (angazovanje) => {
        const { id } = angazovanje;
        const { dodataAngazovanja } = this.state;
        const angazovanja = dodataAngazovanja.filter(angazovanje => angazovanje.id !== id);
        this.setState({
            dodataAngazovanja: angazovanja,
        });
    }

    renderDodataAngazovanja = () => {
        const { dodataAngazovanja } = this.state;
        if (dodataAngazovanja.length === 0) {
            return 'Нема додатих ангажовања.';
        }
        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Наставник</TableCell>
                            <TableCell>Тип активности</TableCell>
                            <TableCell>Право потписа</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodataAngazovanja.map(angazovanje => {
                                const getPodaciONastavniku = () => {
                                    return angazovanje.nastavnik.ime + ' ' + angazovanje.nastavnik.prezime;
                                }

                                const getPodaciOPravuPotpisa = () => {
                                    return angazovanje.pravoPotpisa ? 'Има право потписа' : 'Нема право потписа';
                                }

                                return (
                                    <TableRow key={Math.random() + '-angazovanje'}>
                                        <TableCell>{getPodaciONastavniku()}</TableCell>
                                        <TableCell>{angazovanje.tipAktivnosti.naziv}</TableCell>
                                        <TableCell>{getPodaciOPravuPotpisa()}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={() => this.obrisiDodatoAngazovanje(angazovanje)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    sacuvajStavku = async () => {
        const { izabraniPredmet, dodataLiteratura, dodataAngazovanja, dodateStavkePokrivenostiNastave } = this.state;
        const { stavke } = this.state.pokrivenostNastave;

        if (izabraniPredmet === null) {
            alert('Изабери предмет');
            return;
        }

        var predmetPostoji = false;

        dodateStavkePokrivenostiNastave.forEach(stavka => {
            const predmet = stavka.predmet;
            if (predmet.id === izabraniPredmet.id) {
                predmetPostoji = true;
            }
        });

        stavke.forEach(stavka => {
            const { predmet } = stavka;
            if (predmet.id === izabraniPredmet.id) {
                predmetPostoji = true;
            }
        });

        if (predmetPostoji) {
            alert('Предмет ' + izabraniPredmet.naziv + ' је већ изабран.');
            return;
        }

        const stavka = {
            predmet: izabraniPredmet,
            angazovanja: dodataAngazovanja,
            literatura: dodataLiteratura,
        }
        stavka.id = parseInt(Math.random());

        const noveStavke = [...stavke];
        noveStavke.push(stavka);

        const { pokrivenostNastave } = this.state;
        const novaPokrivenostNastave = {
            ...pokrivenostNastave
        }
        novaPokrivenostNastave.stavke = [...noveStavke];

        await this.setState({
            pokrivenostNastave: novaPokrivenostNastave,
        });


    }

    renderKreiranjeStavkePokrivenosti = () => {
        return (
            <Paper style={{ marginTop: '20px', padding: '20px' }}>
                <h2>Додај ставку</h2>

                {this.renderIzborPredmeta()}


                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborAngazovanja()}
                    </Col>

                    <Col>
                        <h4>Додата анагажовања</h4>

                        {this.renderDodataAngazovanja()}
                    </Col>
                </Row>

                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborLiterature()}
                    </Col>
                    <Col>
                        <h4>Изабрана литература</h4>

                        {this.renderIzabranaLiteratura()}
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Button variant="outlined" fullWidth onClick={this.sacuvajStavku}>
                            Сачувај ставку <SaveIcon />
                        </Button>
                    </Col>
                    <Col></Col>
                </Row>
            </Paper>
        );
    }

    obrisiIzabranuLiteraturu = (literatura) => {
        const { dodataLiteratura } = this.state;
        const dodataLiteraturaNovo = dodataLiteratura.filter(lit => lit.id !== literatura.id);
        this.setState({
            dodataLiteratura: dodataLiteraturaNovo
        });
    }

    renderIzabranaLiteratura = () => {
        const { dodataLiteratura } = this.state;

        if (dodataLiteratura.length === 0) {
            return (
                <span>
                    Литература није изабрана.
                </span>
            );
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Назив</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodataLiteratura.map(literatura => {
                                return (
                                    <TableRow key={literatura.id}>
                                        <TableCell>{literatura.naziv}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={() => this.obrisiIzabranuLiteraturu(literatura)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    izaberiLiteraturu = (e) => {
        const { literature } = this.state.ucitaniPodaci;
        if (literature == null) {
            return;
        }
        const literaturaId = e.target.value;
        const literatura = literature.find((literatura) => literatura.id === literaturaId);

        if (literatura != null) {
            this.setState({
                izabranaLiteratura: literatura,
            });
        }
    }

    getIzabranaLiteraturaValue = () => {
        const { izabranaLiteratura } = this.state;
        if (izabranaLiteratura === null) {
            return 'Изабери литературу';
        }
        return izabranaLiteratura.naziv;
    }

    dodajLiteraturu = () => {
        const { izabranaLiteratura } = this.state;

        if (izabranaLiteratura === null) {
            alert('Изабери литературу');
            return;
        }

        const { dodataLiteratura } = this.state;
        const postoji = dodataLiteratura.find(literatura => literatura.id === izabranaLiteratura.id);

        if (postoji === null || postoji === undefined) {
            const literature = [...dodataLiteratura];
            literature.push(izabranaLiteratura);

            this.setState({
                dodataLiteratura: literature
            });
        } else {
            alert('Изабрана литература је већ додата.');
        }
    }

    renderIzborLiterature = () => {
        const { literature } = this.state.ucitaniPodaci;
        if (literature == null) {
            return null;
        }
        return (
            <div>
                <h4>Избор литературе</h4>

                <Row>
                    <Col xs={colXs4}>
                        Назив литературе
                    </Col>
                    <Col>
                        <FormControl fullWidth>
                            <Select
                                value={this.getIzabranaLiteraturaValue()}
                                onChange={this.izaberiLiteraturu}
                                renderValue={() => this.getIzabranaLiteraturaValue()}>

                                {
                                    literature.map(literatura => {
                                        return (
                                            <MenuItem key={literatura.id} value={literatura.id}>
                                                {literatura.naziv}
                                            </MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Col>
                    <Col xs={2}>
                        <Button variant="outlined" size="small" onClick={this.dodajLiteraturu}>
                            Додај
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }

    renderDodajStavku = () => {
        return (
            <div>
                <Row>
                    <Col>
                        {
                            (this.state.pokrivenostNastave === null) ? null : this.renderKreiranjeStavkePokrivenosti()
                        }
                    </Col>
                </Row>
            </div>
        );
    }

    getIzabraniPredmetZaStavkuValue = (stavka) => {
        return stavka.predmet.naziv;
    }

    izaberiPredmetZaStavku = (e) => {
        const { stavkaZaIzmenu } = this.state;

        const id = e.target.value;
        const { predmeti } = this.state.ucitaniPodaci;
        const predmet = predmeti.find(predmet => predmet.id === id);

        const stavka = {
            ...stavkaZaIzmenu
        }

        stavka.predmet = {
            ...predmet
        }


        if (predmet !== null && predmet !== undefined) {
            this.setState({
                stavkaZaIzmenu: stavka,
            });
        }
    }

    renderIzborPredmetaZaStavku = (stavka) => {
        const { predmeti } = this.state.ucitaniPodaci;
        if (predmeti === null) {
            return null;
        }
        return (
            <Row>
                <Col xs={3}>
                    Предмет
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniPredmetZaStavkuValue(stavka)}
                            onChange={this.izaberiPredmetZaStavku}
                            renderValue={() => this.getIzabraniPredmetZaStavkuValue(stavka)}>
                            {
                                predmeti.map(predmet => {
                                    return (
                                        <MenuItem key={predmet.id} value={predmet.id}>
                                            {predmet.naziv}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    renderDodataAngazovanjaZaStavku = (stavka) => {
        const { angazovanja } = stavka;
        if (angazovanja.length === 0) {
            return 'Нема додатих ангажовања.';
        }
        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Наставник</TableCell>
                            <TableCell>Тип активности</TableCell>
                            <TableCell>Право потписа</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            angazovanja.map(angazovanje => {
                                const getPodaciONastavniku = () => {
                                    return angazovanje.nastavnik.ime + ' ' + angazovanje.nastavnik.prezime;
                                }

                                const getPodaciOPravuPotpisa = () => {
                                    return angazovanje.pravoPotpisa ? 'Има право потписа' : 'Нема право потписа';
                                }

                                const obrisiDodatoAngazovanje = (angazovanje) => {

                                    const novaAngazovanja = angazovanja.filter(ang => ang !== angazovanje);

                                    const novaStavka = {
                                        ...stavka
                                    }

                                    novaStavka.angazovanja = [...novaAngazovanja];

                                    this.setState({
                                        stavkaZaIzmenu: novaStavka,
                                    });
                                }

                                return (
                                    <TableRow key={Math.random() + '-angazovanje'}>
                                        <TableCell>{getPodaciONastavniku()}</TableCell>
                                        <TableCell>{angazovanje.tipAktivnosti.naziv}</TableCell>
                                        <TableCell>{getPodaciOPravuPotpisa()}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={() => obrisiDodatoAngazovanje(angazovanje)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    renderIzborAngazovanjaZaStavku = (stavka) => {
        return (
            <div>
                <h4>Додавање анагажовања</h4>

                {this.renderIzborNastavnika()}

                {this.renderIzborTipaAktivnosti()}

                {this.renderPravoPotpisa('stavkaZaIzmenu')}
            </div>
        );
    }

    dodajLiteraturuZaStavku = () => {
        const { izabranaLiteratura } = this.state;

        if (izabranaLiteratura === null) {
            alert('Изабери литературу');
            return;
        }

        const { literatura } = this.state.stavkaZaIzmenu;
        const postoji = literatura.find(lit => lit.id === izabranaLiteratura.id);

        if (postoji === null || postoji === undefined) {
            const { stavkaZaIzmenu } = this.state;

            const novaLiteratura = [...literatura];
            novaLiteratura.push(izabranaLiteratura);

            const novaStavka = {
                ...stavkaZaIzmenu
            }
            novaStavka.literatura = [...novaLiteratura];

            this.setState({
                stavkaZaIzmenu: novaStavka
            });
        } else {
            alert('Изабрана литература је већ додата.');
        }
    }

    renderIzborLiteratureZaStavku = (stavka) => {
        const { literature } = this.state.ucitaniPodaci;
        if (literature == null) {
            return null;
        }
        return (
            <div>
                <h4>Избор литературе</h4>

                <Row>
                    <Col xs={colXs4}>
                        Назив литературе
                    </Col>
                    <Col>
                        <FormControl fullWidth>
                            <Select
                                value={this.getIzabranaLiteraturaValue()}
                                onChange={this.izaberiLiteraturu}
                                renderValue={() => this.getIzabranaLiteraturaValue()}>

                                {
                                    literature.map(literatura => {
                                        return (
                                            <MenuItem key={literatura.id} value={literatura.id}>
                                                {literatura.naziv}
                                            </MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Col>
                    <Col xs={2}>
                        <Button variant="outlined" size="small" onClick={this.dodajLiteraturuZaStavku}>
                            Додај
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }

    renderIzabranaLiteraturaZaStavku = (stavka) => {
        const { literatura } = stavka;

        if (literatura.length === 0) {
            return (
                <span>
                    Литература није изабрана.
                </span>
            );
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Назив</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            literatura.map(literatura => {

                                const obrisiIzabranuLiteraturu = () => {
                                    const dodataLiteratura = stavka.literatura;
                                    const dodataLiteraturaNovo = dodataLiteratura.filter(lit => lit.id !== literatura.id);
                                    const novaStavka = {
                                        ...stavka
                                    }

                                    novaStavka.literatura = [...dodataLiteraturaNovo];

                                    this.setState({
                                        stavkaZaIzmenu: novaStavka
                                    });
                                }

                                return (
                                    <TableRow key={literatura.id}>
                                        <TableCell>{literatura.naziv}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={obrisiIzabranuLiteraturu}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    sacuvajStavkuZaIzmenu = async () => {
        const dodateStavkePokrivenostiNastave = this.state.pokrivenostNastave.stavke;
        const { stavkaZaIzmenu } = this.state;
        const izabraniPredmet = stavkaZaIzmenu.predmet;

        if (izabraniPredmet === null) {
            alert('Изабери предмет');
            return;
        }

        var predmetPostoji = false;

        dodateStavkePokrivenostiNastave.forEach(stavka => {
            const predmet = stavka.predmet;
            if (predmet.id === izabraniPredmet.id && stavka.id !== stavkaZaIzmenu.id) {
                predmetPostoji = true;
            }
        });

        if (predmetPostoji) {
            alert('Предмет ' + izabraniPredmet.naziv + ' је већ изабран.');
            return;
        }

        const stavka = {
            ...stavkaZaIzmenu
        }

        const stavke = dodateStavkePokrivenostiNastave.filter(stavka => stavka.id !== stavkaZaIzmenu.id);
        stavke.push(stavka);

        const { pokrivenostNastave } = this.state;
        const novaPokrivenostNastave = {
            ...pokrivenostNastave
        }
        novaPokrivenostNastave.stavke = stavke;

        await this.setState({
            pokrivenostNastave: novaPokrivenostNastave,
        });
    }

    renderStavkaZaIzmenu = () => {
        const { stavkaZaIzmenu } = this.state;
        if (stavkaZaIzmenu === null) {
            return 'Изабери ставку за измену';
        }

        return (
            <Paper style={{ marginTop: '20px', padding: '20px' }}>
                <h2>Измени ставку</h2>

                {this.renderIzborPredmetaZaStavku(stavkaZaIzmenu)}


                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborAngazovanjaZaStavku(stavkaZaIzmenu)}
                    </Col>

                    <Col>
                        <h4>Додата анагажовања</h4>

                        {this.renderDodataAngazovanjaZaStavku(stavkaZaIzmenu)}
                    </Col>
                </Row>

                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborLiteratureZaStavku(stavkaZaIzmenu)}
                    </Col>
                    <Col>
                        <h4>Изабрана литература</h4>

                        {this.renderIzabranaLiteraturaZaStavku(stavkaZaIzmenu)}
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Button variant="outlined" fullWidth onClick={this.sacuvajStavkuZaIzmenu}>
                            Сачувај ставку <SaveIcon />
                        </Button>
                    </Col>
                    <Col></Col>
                </Row>
            </Paper>
        );

    }

    renderForm = () => {
        const { hasError, tab } = this.state;

        if (hasError) {
            return 'Дошло је до грешке приликом учитавања потребних података.';
        }

        return (
            <div>
                <Row>
                    <Col>
                        {
                            this.renderPokrivenostNastave()
                        }
                    </Col>
                </Row>

                <Tabs value={tab} onChange={() => {
                    this.setState({
                        tab: this.state.tab === 0 ? 1 : 0,
                    });
                }}>
                    <Tab label="Додај ставку" />
                    <Tab label="Измени ставку" />
                </Tabs>

                {tab === 0 && this.renderDodajStavku()}
                {tab === 1 && this.renderStavkaZaIzmenu()}

            </div>
        );
    }

    render() {
        return (
            <div>
                <Header text='Измена покривености наставе' />

                {
                    this.state.message
                }

                {
                    this.renderForm()
                }
            </div>
        );
    }
}

const colXs4 = 3;

export default PokrivenostNastaveIzmeni;