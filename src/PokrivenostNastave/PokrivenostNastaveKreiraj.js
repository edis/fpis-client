import React, { Component } from 'react';
import Form from 'react-bootstrap/FormGroup';
import { Select, MenuItem, TextField, Paper, FormControl, Button, Checkbox, Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';
import axios from 'axios';
import { BASE_URL, BEAN_NASTAVNI_PLAN, BEAN_LITERATURA, BEAN_NASTAVNIK, BEAN_PREDMET, BEAN_TIP_AKTIVNOSTI, POKRIVENOST_NASTAVE } from '../util/apiUrls';
import { Row, Col, Alert } from 'react-bootstrap';
import Header from '../Header/Header';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined'
import SaveIcon from '@material-ui/icons/Save'

class PokrivenostNastaveKreiraj extends Component {
    state = {
        loading: true,
        hasError: false,
        message: null,

        kreiranPokrivenostNastave: false,

        ucitaniPodaci: {
            nastavniPlanovi: null,
            literature: null,
            nastavnici: null,
            predmeti: null,
            tipoviAktivnosti: null,
        },

        izabraniNastavniPlan: null,
        izabranaLiteratura: null,
        izabraniNastavnik: null,
        izabraniPredmet: null,
        izabraniTipAktivnosti: null,
        pravoPotpisa: false,

        pokrivenostNastave: null,

        dodataLiteratura: [],
        dodataAngazovanja: [],
        dodateStavkePokrivenostiNastave: [],

        godina: new Date().getFullYear() + '/' + (new Date().getFullYear() + 1),
        datum: null,
    }

    ucitaj = (naziv, uri) => {
        axios.get(BASE_URL + uri).then(resp => {
            const { status } = resp.data;
            if (status === 200) {
                const { ucitaniPodaci } = this.state;
                ucitaniPodaci[naziv] = resp.data.data;
                this.setState({
                    ucitaniPodaci,
                });
            } else {
                this.setLoadingError();
            }
        }).catch(err => {
            this.setLoadingError();
        });
    }

    setLoadingError = () => {
        this.setState({
            hasError: true,
            loading: false,
        });
    }

    componentDidMount() {
        this.setState({
            datum: this.getTekuciDatum(),
        });
        try {
            this.ucitaj('nastavniPlanovi', BEAN_NASTAVNI_PLAN);
            this.ucitaj('literature', BEAN_LITERATURA);
            this.ucitaj('nastavnici', BEAN_NASTAVNIK);
            this.ucitaj('predmeti', BEAN_PREDMET);
            this.ucitaj('tipoviAktivnosti', BEAN_TIP_AKTIVNOSTI);
        } catch (error) {
            this.setLoadingError();
        }

    }

    showMessage = (msg, type = 'success') => {
        if (msg == null) {
            this.setState({
                message: null,
            })
        } else {
            this.setState({
                message: (
                    <Alert variant={type}>
                        {msg}
                    </Alert>
                )
            });
        }
    }

    sacuvajPokrivenostNastave = () => {
        const { pokrivenostNastave, dodateStavkePokrivenostiNastave } = this.state;

        pokrivenostNastave['stavke'] = dodateStavkePokrivenostiNastave;

        axios.post(BASE_URL + POKRIVENOST_NASTAVE, pokrivenostNastave)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.showMessage('Покривеност наставе је успешно креирана');
                } else {
                    this.showMessage(resp.data.message, 'danger');
                }
            }).catch(err => {
                alert(err);
            });
    }

    izaberiNastavniPlan = (e) => {
        const { nastavniPlanovi } = this.state.ucitaniPodaci;
        const nastavniPlan = nastavniPlanovi.find((el) => el.id === e.target.value);

        if (nastavniPlan) {
            this.setState({
                izabraniNastavniPlan: { ...nastavniPlan },
            });
        }
    }

    getNastavniPlanValue = (nastavniPlan) => {
        if (nastavniPlan === null) {
            return '';
        }
        return nastavniPlan.datumOd + ' - ' + nastavniPlan.datumDo;
    }

    setTekucaGodina = () => {
        const year = new Date().getFullYear();
        this.setState({
            godina: year + '/' + (year + 1),
        })
    }

    renderIzborNastavnogPlana = () => {
        const { izabraniNastavniPlan } = this.state;
        const { nastavniPlanovi } = this.state.ucitaniPodaci;

        if (nastavniPlanovi === null) {
            return null;
        }

        return (
            <Row>
                <Col xs={3}>
                    Наставни план
                </Col>
                <Col xs={9}>
                    <FormControl fullWidth>
                        <Select
                            value={this.getNastavniPlanValue(izabraniNastavniPlan)}
                            onChange={this.izaberiNastavniPlan}
                            renderValue={() => this.getNastavniPlanValue(izabraniNastavniPlan)}>
                            {
                                nastavniPlanovi.map(nastavniPlan => {
                                    return (
                                        <MenuItem key={nastavniPlan.id} value={nastavniPlan.id}>
                                            {this.getNastavniPlanValue(nastavniPlan)}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    renderUnosGodine = () => {
        return (
            <Row>
                <Col xs={3}>
                    Година
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <TextField
                            label='Година'
                            variant="outlined"
                            value={this.state.godina}
                            onChange={(e) => this.setState({ godina: e.target.value })}
                        />
                    </FormControl>
                </Col>
                <Col>
                    <Button size="small" variant="text" onClick={this.setTekucaGodina}>
                        Текућа година
                    </Button>
                </Col>
            </Row>
        );
    }

    getTekuciDatum = () => {
        const tekuciDatum = new Date();
        const month = tekuciDatum.getMonth() + 1;
        const date = tekuciDatum.getDate();
        return tekuciDatum.getFullYear() + '-' + ((month < 10) ? '0' + month : month) + '-' + ((date < 10) ? ('0'+date) : date);
    }

    renderIzborDatuma = () => {


        return (
            <Row>
                <Col xs={3}>
                    Датум
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <TextField
                            id="date"
                            label="Датум"
                            type="date"
                            defaultValue={this.getTekuciDatum()}
                            onChange={(e) => this.setState({ datum: e.target.value })}
                        />
                    </FormControl>
                </Col>
            </Row>
        );
    }

    napraviPokrivenostNastave = () => {
        const { izabraniNastavniPlan, godina, datum } = this.state;

        if (izabraniNastavniPlan === null || godina === null || datum === null || godina.toString().trim() === '' || datum.trim() === '') {
            alert('Сва поља су обавезна');
            return;
        }

        const pokrivenostNastave = {
            nastavniPlan: izabraniNastavniPlan,
            godina,
            datum,
        }

        this.setState({
            pokrivenostNastave: pokrivenostNastave,
            kreiranPokrivenostNastave: true,
        });
    }

    getPredmetValue = (predmet) => {
        if (predmet === null) {
            return '';
        }
        return predmet.naziv;
    }

    getNastavniciValue = (angazovanja) => {
        var value = '';
        angazovanja.forEach(angazovanje => {
            value += angazovanje.nastavnik.ime + ' ' + angazovanje.nastavnik.prezime + ' (' + angazovanje.tipAktivnosti.naziv + ')';
        });
        return value.length > 50 ? <div title={value}>{value.substr(0, 47)}...</div> : value;
    }

    getLiteraturaValue = (literature) => {
        var value = '';
        literature.forEach(literatura => {
            value += literatura.naziv + ' ';
        });
        return value.length > 50 ? <div title={value}>{value.substr(0, 47)}...</div> : value;
    }

    renderNapravljeneStavke = () => {
        const { dodateStavkePokrivenostiNastave } = this.state;
        if (dodateStavkePokrivenostiNastave === null || dodateStavkePokrivenostiNastave.length === 0) {
            return null;
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Предмет</TableCell>
                            <TableCell>Наставници</TableCell>
                            <TableCell>Литература</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodateStavkePokrivenostiNastave.map(stavka => {
                                return (
                                    <TableRow key={Math.random()}>
                                        <TableCell>{this.getPredmetValue(stavka.predmet)}</TableCell>
                                        <TableCell>{this.getNastavniciValue(stavka.angazovanja)}</TableCell>
                                        <TableCell>{this.getLiteraturaValue(stavka.literatura)}</TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    renderNapravljenPokrivenostNastave = () => {
        const { pokrivenostNastave } = this.state;
        if (pokrivenostNastave === null) {
            return null;
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Наставни план</TableCell>
                            <TableCell>Година</TableCell>
                            <TableCell>Датум</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>{this.getNastavniPlanValue(pokrivenostNastave.nastavniPlan)}</TableCell>
                            <TableCell>{pokrivenostNastave.godina}</TableCell>
                            <TableCell>{pokrivenostNastave.datum}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                {this.renderNapravljeneStavke()}

                <Button variant="outlined" style={{ margin: '20px' }} onClick={this.sacuvajPokrivenostNastave}>
                    <SaveIcon />
                </Button>
            </div>
        );
    }

    renderPokrivenostNastave = () => {
        const marginBottom20 = {
            marginBottom: '20px',
        }
        const { kreiranPokrivenostNastave } = this.state;

        if (kreiranPokrivenostNastave) {
            return (
                <Row>
                    <Col>
                        {this.renderNapravljenPokrivenostNastave()}
                    </Col>
                </Row>
            );
        }

        return (
            <Paper style={{ padding: '20px' }}>
                <h2>
                    Покривеност наставе
                </h2>
                <Row>
                    <Col>
                        <div style={marginBottom20}>
                            {
                                this.renderIzborNastavnogPlana()
                            }
                        </div>

                        <div style={marginBottom20}>
                            {
                                this.renderUnosGodine()
                            }
                        </div>

                        <div style={marginBottom20}>
                            {
                                this.renderIzborDatuma()
                            }
                        </div>
                        {
                            !this.state.kreiranPokrivenostNastave ? (
                                <Button variant="outlined" onClick={this.napraviPokrivenostNastave}>
                                    <SaveIcon />
                                </Button>
                            ) : null
                        }

                    </Col>
                    <Col>
                        {this.renderNapravljenPokrivenostNastave()}
                    </Col>
                </Row>
            </Paper>
        );
    }

    getIzabraniPredmetValue = () => {
        const { izabraniPredmet } = this.state;
        if (izabraniPredmet === null) {
            return 'Изабери предмет';
        }
        return this.getPredmetValue(izabraniPredmet);
    }

    izaberiPredmet = (e) => {
        const id = e.target.value;
        const { predmeti } = this.state.ucitaniPodaci;
        const predmet = predmeti.find(predmet => predmet.id === id);
        if (predmet !== null && predmet !== undefined) {
            this.setState({
                izabraniPredmet: predmet,
            });
        }
    }

    renderIzborPredmeta = () => {
        const { predmeti } = this.state.ucitaniPodaci;
        if (predmeti === null) {
            return null;
        }
        return (
            <Row>
                <Col xs={3}>
                    Предмет
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniPredmetValue()}
                            onChange={this.izaberiPredmet}
                            renderValue={this.getIzabraniPredmetValue}>
                            {
                                predmeti.map(predmet => {
                                    return (
                                        <MenuItem key={predmet.id} value={predmet.id}>
                                            {predmet.naziv}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    getIzabraniNastavnikValue = () => {
        const { izabraniNastavnik } = this.state;
        if (izabraniNastavnik === null) {
            return 'Изабери наставника';
        }
        return izabraniNastavnik.ime + ' ' + izabraniNastavnik.prezime + ' (' + izabraniNastavnik.jmbg + ')';
    }

    izaberiNastavnika = (e) => {
        const { nastavnici } = this.state.ucitaniPodaci;
        const id = e.target.value;
        const nastavnik = nastavnici.find((nastavnik) => nastavnik.id === id);
        if (nastavnik !== null && nastavnik !== undefined) {
            this.setState({
                izabraniNastavnik: nastavnik,
            });
        }
    }

    renderIzborNastavnika = () => {
        const { nastavnici } = this.state.ucitaniPodaci;
        if (nastavnici === null) {
            return null;
        }

        return (
            <Row>
                <Col xs={colXs4}>
                    Наставник
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniNastavnikValue()}
                            onChange={this.izaberiNastavnika}
                            renderValue={this.getIzabraniNastavnikValue}>
                            {
                                nastavnici.map(nastavnik => {
                                    return (
                                        <MenuItem key={nastavnik.id} value={nastavnik.id}>
                                            {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    getIzabraniTipAktivnostiValue = () => {
        const { izabraniTipAktivnosti } = this.state;
        if (izabraniTipAktivnosti === null) {
            return 'Изабери тип активности';
        }
        const obaveznaPrijava = (izabraniTipAktivnosti.obaveznaPrijava === 0) ? 'пријава није обавезна' : 'пријава је обавезна';
        return izabraniTipAktivnosti.naziv + ' (' + obaveznaPrijava + ')';
    }

    izaberiTipAktivnosti = (e) => {
        const id = e.target.value;
        const { tipoviAktivnosti } = this.state.ucitaniPodaci;
        const tipAktivnosti = tipoviAktivnosti.find(ta => ta.id === id);
        if (tipAktivnosti !== null && tipAktivnosti !== undefined) {
            this.setState({
                izabraniTipAktivnosti: tipAktivnosti,
            });
        }
    }

    renderIzborTipaAktivnosti = () => {
        const { tipoviAktivnosti } = this.state.ucitaniPodaci;
        if (tipoviAktivnosti === null) {
            return null;
        }

        return (
            <Row>
                <Col xs={colXs4}>
                    Тип активности
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Select
                            value={this.getIzabraniTipAktivnostiValue()}
                            onChange={this.izaberiTipAktivnosti}
                            renderValue={this.getIzabraniTipAktivnostiValue}>
                            {
                                tipoviAktivnosti.map(tipAktivnosti => {
                                    return (
                                        <MenuItem key={tipAktivnosti.id} value={tipAktivnosti.id}>
                                            {tipAktivnosti.naziv}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Col>
            </Row>
        );
    }

    renderPravoPotpisa = () => {
        const { pravoPotpisa } = this.state;
        return (
            <Row>
                <Col xs={3}>
                    Право потписа
                </Col>
                <Col>
                    <FormControl fullWidth>
                        <Checkbox
                            checked={pravoPotpisa}
                            onChange={() => this.setState({ pravoPotpisa: !pravoPotpisa })}
                        />
                    </FormControl>
                </Col>
                <Col>
                    <Button variant="outlined" size="small" onClick={this.dodajAngazovanje}>
                        Додај ангажовање
                    </Button>
                </Col>
            </Row>
        );
    }

    dodajAngazovanje = () => {
        const { izabraniNastavnik, izabraniTipAktivnosti, pravoPotpisa } = this.state;
        const { dodataAngazovanja } = this.state;

        if (izabraniNastavnik === null) {
            alert('Изабери наставника');
            return;
        }

        if (izabraniTipAktivnosti === null) {
            alert('Изабери тип активности');
            return;
        }

        const angazovanjeId = izabraniNastavnik.id + '' + izabraniTipAktivnosti.id;

        const postojeceAngazovanje = dodataAngazovanja.find(angazovanje => angazovanje.id === parseInt(angazovanjeId));
        if (postojeceAngazovanje !== null && postojeceAngazovanje !== undefined) {
            alert('Ангажовање са изабраним наставником и типом активности већ постоји');
            return;
        }

        const angazovanje = {
            id: parseInt(angazovanjeId),
            nastavnik: izabraniNastavnik,
            tipAktivnosti: izabraniTipAktivnosti,
            pravoPotpisa: pravoPotpisa,
        };

        const angazovanja = [...dodataAngazovanja];
        angazovanja.push(angazovanje);
        this.setState({
            dodataAngazovanja: angazovanja
        });
    }

    renderIzborAngazovanja = () => {
        return (
            <div>
                <h4>Додавање анагажовања</h4>

                {this.renderIzborNastavnika()}

                {this.renderIzborTipaAktivnosti()}

                {this.renderPravoPotpisa()}
            </div>
        );
    }

    obrisiDodatoAngazovanje = (angazovanje) => {
        const { id } = angazovanje;
        const { dodataAngazovanja } = this.state;
        const angazovanja = dodataAngazovanja.filter(angazovanje => angazovanje.id !== id);
        this.setState({
            dodataAngazovanja: angazovanja,
        });
    }

    renderDodataAngazovanja = () => {
        const { dodataAngazovanja } = this.state;
        if (dodataAngazovanja.length === 0) {
            return 'Нема додатих ангажовања.';
        }
        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Наставник</TableCell>
                            <TableCell>Тип активности</TableCell>
                            <TableCell>Право потписа</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodataAngazovanja.map(angazovanje => {
                                const getPodaciONastavniku = () => {
                                    return angazovanje.nastavnik.ime + ' ' + angazovanje.nastavnik.prezime;
                                }

                                const getPodaciOPravuPotpisa = () => {
                                    return angazovanje.pravoPotpisa ? 'Има право потписа' : 'Нема право потписа';
                                }

                                return (
                                    <TableRow key={Math.random() + '-angazovanje'}>
                                        <TableCell>{getPodaciONastavniku()}</TableCell>
                                        <TableCell>{angazovanje.tipAktivnosti.naziv}</TableCell>
                                        <TableCell>{getPodaciOPravuPotpisa()}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={() => this.obrisiDodatoAngazovanje(angazovanje)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    sacuvajStavku = async () => {
        const { izabraniPredmet, dodataLiteratura, dodataAngazovanja, dodateStavkePokrivenostiNastave } = this.state;

        if (izabraniPredmet === null) {
            alert('Изабери предмет');
            return;
        }
        
        var predmetPostoji = false;
        
        dodateStavkePokrivenostiNastave.forEach(stavka => {
            const predmet = stavka.predmet;
            if (predmet.id === izabraniPredmet.id) {
                predmetPostoji = true;
            }
        });
    
        if (predmetPostoji) {
            alert('Предмет ' + izabraniPredmet.naziv + ' је већ изабран.');
            return;
        }

        const stavka = {
            predmet: izabraniPredmet,
            angazovanja: dodataAngazovanja,
            literatura: dodataLiteratura,
        }

        const stavke = [...dodateStavkePokrivenostiNastave];
        stavke.push(stavka);

        await this.setState({
            dodateStavkePokrivenostiNastave: stavke,
        });


    }

    renderStavkaPokrivenosti = () => {
        return (
            <Paper style={{ marginTop: '20px', padding: '20px' }}>
                <h2>Ставке покривености наставе</h2>

                {this.renderIzborPredmeta()}


                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborAngazovanja()}
                    </Col>

                    <Col>
                        <h4>Додата анагажовања</h4>

                        {this.renderDodataAngazovanja()}
                    </Col>
                </Row>

                <Row style={{ marginTop: '20px' }}>
                    <Col>
                        {this.renderIzborLiterature()}
                    </Col>
                    <Col>
                        <h4>Изабрана литература</h4>

                        {this.renderIzabranaLiteratura()}
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Button variant="outlined" fullWidth onClick={this.sacuvajStavku}>
                            Сачувај ставку <SaveIcon />
                        </Button>
                    </Col>
                    <Col></Col>
                </Row>
            </Paper>
        );
    }

    obrisiIzabranuLiteraturu = (literatura) => {
        const { dodataLiteratura } = this.state;
        const dodataLiteraturaNovo = dodataLiteratura.filter(lit => lit.id !== literatura.id);
        this.setState({
            dodataLiteratura: dodataLiteraturaNovo
        });
    }

    renderIzabranaLiteratura = () => {
        const { dodataLiteratura } = this.state;

        if (dodataLiteratura.length === 0) {
            return (
                <span>
                    Литература није изабрана.
                </span>
            );
        }

        return (
            <div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Назив</TableCell>
                            <TableCell>Акција</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            dodataLiteratura.map(literatura => {
                                return (
                                    <TableRow key={literatura.id}>
                                        <TableCell>{literatura.naziv}</TableCell>
                                        <TableCell>
                                            <Button variant="text" size="small" onClick={() => this.obrisiIzabranuLiteraturu(literatura)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    izaberiLiteraturu = (e) => {
        const { literature } = this.state.ucitaniPodaci;
        if (literature == null) {
            return;
        }
        const literaturaId = e.target.value;
        const literatura = literature.find((literatura) => literatura.id === literaturaId);

        if (literatura != null) {
            this.setState({
                izabranaLiteratura: literatura,
            });
        }
    }

    getIzabranaLiteraturaValue = () => {
        const { izabranaLiteratura } = this.state;
        if (izabranaLiteratura === null) {
            return 'Изабери литературу';
        }
        return izabranaLiteratura.naziv;
    }

    dodajLiteraturu = () => {
        const { izabranaLiteratura } = this.state;

        if (izabranaLiteratura === null) {
            alert('Изабери литературу');
            return;
        }

        const { dodataLiteratura } = this.state;
        const postoji = dodataLiteratura.find(literatura => literatura.id === izabranaLiteratura.id);

        if (postoji === null || postoji === undefined) {
            const literature = [...dodataLiteratura];
            literature.push(izabranaLiteratura);

            this.setState({
                dodataLiteratura: literature
            });
        } else {
            alert('Изабрана литература је већ додата.');
        }
    }

    renderIzborLiterature = () => {
        const { literature } = this.state.ucitaniPodaci;
        if (literature == null) {
            return null;
        }
        return (
            <div>
                <h4>Избор литературе</h4>

                <Row>
                    <Col xs={colXs4}>
                        Назив литературе
                    </Col>
                    <Col>
                        <FormControl fullWidth>
                            <Select
                                value={this.getIzabranaLiteraturaValue()}
                                onChange={this.izaberiLiteraturu}
                                renderValue={() => this.getIzabranaLiteraturaValue()}>

                                {
                                    literature.map(literatura => {
                                        return (
                                            <MenuItem key={literatura.id} value={literatura.id}>
                                                {literatura.naziv}
                                            </MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Col>
                    <Col xs={2}>
                        <Button variant="outlined" size="small" onClick={this.dodajLiteraturu}>
                            Додај
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }

    renderForm = () => {
        const { hasError } = this.state;

        if (hasError) {
            return 'Дошло је до грешке приликом учитавања потребних података.';
        }

        return (
            <Form>
                <Row>
                    <Col>
                        {
                            this.renderPokrivenostNastave()
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {
                            (this.state.pokrivenostNastave === null) ? null : this.renderStavkaPokrivenosti()
                        }
                    </Col>
                </Row>
            </Form>
        );
    }

    render() {
        return (
            <div>
                <Header text='Креирање покривености наставе' />

                {
                    this.state.message
                }

                {
                    this.renderForm()
                }
            </div>
        );
    }
}

const colXs4 = 3;

export default PokrivenostNastaveKreiraj;