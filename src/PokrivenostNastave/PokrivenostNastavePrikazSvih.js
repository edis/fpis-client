import React, { Component } from 'react';
import Header from '../Header/Header';
import axios from 'axios';
import { BASE_URL, BEAN_POKRIVENOST_NASTAVE, POKRIVENOST_NASTAVE } from '../util/apiUrls';
import { Button, CircularProgress, Paper, ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails, Table, TableHead, TableRow, TableBody, TableCell } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Row, Col } from 'react-bootstrap'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined'
class PokrivenostNastavePrikazSvih extends Component {

    state = {
        loading: true,
        hasError: false,
        pokrivenostiNastave: [],

    }

    ucitaj = () => {
        this.setState({
            loading: true,
            hasError: false,
            pokrivenostiNastave: [],
        });
        axios.get(BASE_URL + BEAN_POKRIVENOST_NASTAVE)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    this.setState({
                        loading: false,
                        hasError: false,
                        pokrivenostiNastave: resp.data.data,
                    });
                } else {
                    this.setError(resp.data.message);
                }
            }).catch(err => {
                this.setError('Дошло је до грешке приликом учитавања покривености наставе');
            });
    }

    setError = (msg) => {
        this.setState({
            loading: false,
            hasError: true,
            pokrivenostiNastave: [],
        });
    }

    componentDidMount() {
        this.ucitaj();
    }

    navigateToKreirajPokrivenostNastave = () => {
        this.props.history.push('/pokrivenost-nastave/kreiraj');
    }

    renderButtonDodajPokrivenostNastave = () => {
        return (
            <div>
                <Button variant="contained" onClick={this.navigateToKreirajPokrivenostNastave}>
                    Направи покривеност наставе
                </Button>
            </div>
        );
    }

    renderAngazovanjaPredavanja = (angazovanja) => {
        const angazovanjaPredavanja = angazovanja.filter(angazovanje => angazovanje.tipAktivnosti.naziv === 'predavanja');

        if (angazovanjaPredavanja.length === 0) {
            return '-';
        }

        return angazovanjaPredavanja.map(angazovanje => {
            const { nastavnik } = angazovanje;
            return (
                <div key={angazovanje.id + '-' + nastavnik.id + '-' + Math.random()}>
                    {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                </div>
            );
        });
    }

    renderAngazovanjaVezbe = (angazovanja) => {
        const angazovanjaVezbe = angazovanja.filter(angazovanje => angazovanje.tipAktivnosti.naziv === 'vezbe');

        if (angazovanjaVezbe.length === 0) {
            return '-';
        }

        return angazovanjaVezbe.map(angazovanje => {
            const { nastavnik } = angazovanje;
            return (
                <div key={angazovanje.id + '-' + nastavnik.id + '-' + Math.random()}>
                    {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                </div>
            );
        });
    }

    renderAngazovanjaLaboratorijskeVezbe = (angazovanja) => {
        const angazovanjaLabVezbe = angazovanja.filter(angazovanje => angazovanje.tipAktivnosti.naziv === 'laboratorijske vezbe');

        if (angazovanjaLabVezbe.length === 0) {
            return '-';
        }

        return angazovanjaLabVezbe.map(angazovanje => {
            const { nastavnik } = angazovanje;
            return (
                <div key={angazovanje.id + '-' + nastavnik.id + '-' + Math.random()}>
                    {nastavnik.ime + ' ' + nastavnik.prezime + ' (' + nastavnik.jmbg + ')'}
                </div>
            );
        });
    }

    renderLiteratura = (literature) => {

        return literature.map(literatura => {
            return (
                <div key={literatura.id + '-' + literatura.predmet.id + '-' + Math.random()}>
                    {literatura.naziv}
                </div>
            );
        });
    }

    renderStavke = (stavke) => {
        if (stavke === null) {
            return null;
        }

        return (
            <div style={{ maxHeight: '380px', overflow: 'auto' }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Назив предмета</TableCell>
                            <TableCell>Предавања</TableCell>
                            <TableCell>Вежбе</TableCell>
                            <TableCell>Лабораторијске вежбе</TableCell>
                            <TableCell>Литература</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            stavke.map(stavka => {
                                const { angazovanja, literatura } = stavka;
                                return (
                                    <TableRow key={stavka.id}>
                                        <TableCell>
                                            {stavka.predmet.naziv}
                                        </TableCell>
                                        <TableCell>
                                            {this.renderAngazovanjaPredavanja(angazovanja)}
                                        </TableCell>
                                        <TableCell>
                                            {this.renderAngazovanjaVezbe(angazovanja)}
                                        </TableCell>
                                        <TableCell>
                                            {this.renderAngazovanjaLaboratorijskeVezbe(angazovanja)}
                                        </TableCell>
                                        <TableCell>
                                            {this.renderLiteratura(literatura)}
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        );
    }

    obrisiPokrivenostNastave = (pokrivenostNastave) => {
        axios.delete(BASE_URL + POKRIVENOST_NASTAVE + '/' + pokrivenostNastave.id)
            .then(resp => {
                const { status } = resp.data;
                if (status === 200) {
                    console.log(status);
                    this.ucitaj();
                    this.props.history.push('/pokrivenost-nastave')
                } else {
                    alert(resp.data.message);
                }
            }).catch(err => {
                alert(err);
            })
    }

    renderPokrivenostiNastave = () => {
        const { loading, hasError, pokrivenostiNastave } = this.state;

        if (loading) {
            return <CircularProgress />
        }

        if (hasError) {
            return 'Дошло је до грешке приликом учитавања података.';
        }

        return (
            <div>
                {
                    pokrivenostiNastave.map(pokrivenostNastave => {
                        return (
                            <ExpansionPanel key={pokrivenostNastave.id}>
                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography>
                                        {pokrivenostNastave.godina}
                                        <span style={{ marginLeft: '50px', color: '#8c8c8c' }}>{pokrivenostNastave.datum}</span>
                                    </Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <div>
                                        <h3 className="center">Покривеност наставе за школску {pokrivenostNastave.godina} годину</h3>

                                        <Button variant="contained" onClick={() => {
                                            this.props.history.push('/pokrivenost-nastave/izmeni/' + pokrivenostNastave.id)
                                        }}>
                                            <EditIcon />
                                        </Button>

                                        <Button style={{ marginLeft: '20px' }} variant="contained" onClick={() => this.obrisiPokrivenostNastave(pokrivenostNastave)}>
                                            <DeleteIcon />
                                        </Button>

                                        <Row>
                                            <Col>
                                                {
                                                    this.renderStavke(pokrivenostNastave.stavke)
                                                }
                                            </Col>
                                        </Row>
                                    </div>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        );
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div>
                <Header text='Приказ покривености наставе' />
                {
                    this.renderButtonDodajPokrivenostNastave()
                }

                <Paper style={{ marginTop: '30px' }}>
                    {
                        this.renderPokrivenostiNastave()
                    }
                </Paper>

            </div>
        );
    }
}

export default PokrivenostNastavePrikazSvih;