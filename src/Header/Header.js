import React, { Component } from 'react'
import { Jumbotron } from 'react-bootstrap'
import PropTypes from 'prop-types'

class Header extends Component {
    state = {}
    render() {
        return (
            <Jumbotron>
                <h1>{this.props.text}</h1>
            </Jumbotron>
        );
    }
}

Header.propTypes = {
    text: PropTypes.string.isRequired
}

export default Header;